<?php

$es = array(
   
    'name' => 'Nombre',   
    'user_list' => 'Lista de usuarios',
    'edit' => 'Editar',
    'delete' => 'Borrar',
    'new_user' => 'Regístrese',
    'index' => 'Inicio',
    'help' => 'Ayuda',
    'user'=> 'Usuario',
    'error_password' => 'La contraseña debe tener entre 6 y 20 caracteres',
    'password' => 'Contraseña',
    'role' => 'Rol',
    'operations' => 'Operaciones',
    //controles select
    'select_one' => 'Seleccionar uno  ------------',
    'connect' => 'Conectar',
    'login' => 'Acceder',
    'unlogin' => 'Desconectar',
    'language' => 'Español',
    'online_shop' => 'Tienda Online',
    'welcome' => 'Bienvenidos a la tienda OnLine.',
    'discounts' => '¡¡Hay descuentos especiales a partir de 500€!!',
    'product_list' => 'Lista de productos',
    'product' => 'Productos',
    'Bienvenido:' => 'Bienvenido:',
    'carrito' => 'Carrito',
    'my_profile' => 'Mi perfil',
    'orders' => 'Número de pedidos',
    'repetido' => 'El usuario ya existe',
    'edicion' => ' Edición de usuarios: ',
    'nuevoPass' => 'Nuevo password',
    'edicionUsuario' => 'Edición de usuarios',
    'alta' => 'Alta de usuarios',
    'weAre' => 'Estamos en el',
    'code' => 'Código de error: '
   
);