<?php

$en = array(
  
    'name' => 'Name',   
    'user_list' => 'User list',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'new_user' => 'New user',
    'index' => 'Index',
    'help' => 'Help',
    'user'=> 'User',
    'error_password' => 'The password must be between 6 and 20 characters',
    //
    'password' => 'Password',
    'role' => 'Role',
    'operations' => 'Operations',
    //controles select
    'select_one' => 'select one   ------------',
    'connect' => 'Connect',
    'login' => 'Login',
    'unlogin' => 'Unlogin',
    
    'language' => 'English',
    'online_shop' => 'Online Shop',
    'welcome' => 'Welcome to the onLine Shop.',
    'discounts' => 'There are special discounts from 500 € onwards!!',
    'product_list' => 'Product List',
    'product' => 'Products',
    'Bienvenido:' => 'Welcome:',
    'carrito' => 'Shopping cart',
    'my_profile' => 'My profile',
    'repetido' => 'The user all ready exist',
    'edicion' => ' User Edit: ',
    'nuevoPass' => 'New Password',
    'edicionUsuario' => 'Users Edit',
    'alta' => 'User Registration',
    'weAre' => 'We are in the',
    'code' => 'Error Code:'

);