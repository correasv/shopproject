$(document).ready(function () {
    var loadDoc = function (number) {
        var url = 'http://localhost/Proyectotienda/es/product/ajaxPageData/' + number;

        $.post(url, function (data) {
            var currentPage = data[0];
            var pages = data[1];
            var rows = data[2];
            var accesssLevel = data[3];
            $("#tbodyList").empty();
           if( accesssLevel ==1){
               $("#tfootList").empty();
                var newRows = "";               
                for (var i = 0; i < rows.length; i++) { //

                    newRows += "<tr id='" + rows[i].id + "'>";                
                    newRows += "<td class='cell'><div>" + rows[i].codigo +  "</div>";
                    newRows += "<td class='cell'><div>" + rows[i].nombre + "</div>";
                    newRows += "<td class='cell'><div>" + rows[i].precio + "</div>";
                    newRows += "<td class='cell'><div>" + rows[i].existencia + "</div>";                    
                }
                document.getElementById("tbodyList").innerHTML = newRows;
               
           } else if( accesssLevel ==2){
               $("#tfootList").empty();
                var newRows = "";               
                for (var i = 0; i < rows.length; i++) { //

                    newRows += "<tr id='" + rows[i].id + "'>";                
                    newRows += "<td class='cell'><div>" + rows[i].codigo +  "</div>";
                    newRows += "<td class='cell'><div>" + rows[i].nombre + "</div>";
                    newRows += "<td class='cell'><div>" + rows[i].precio + "</div>";
                    newRows += "<td class='cell'><div>" + rows[i].existencia + "</div>";
                    newRows += '<td id="' + rows[i].id + '"><a id="' + rows[i].id + '" precio="' + rows[i].precio + '" nombre="' + rows[i].nombre + '" class="boton4" href="#">COMPRAR</a></td></tr>';
                }
                document.getElementById("tbodyList").innerHTML = newRows;
            }else{
                
                if(rows.length == 0){
                    loadDoc(currentPage-1);
                }else{                
                    var newRows = "";
                    newRows += "<input id='oculto' value='"+ currentPage +"' type='hidden'>";
                    newRows += "<input id='oculto2' value='"+ pages +"' type='hidden'>";
                    for (var i = 0; i < rows.length; i++) { //

                        newRows += "<tr class='cell' id='" + rows[i].id + "'>";                
                        newRows += "<td class='cell'><input type='text' class='cell' name='codigo' oldValue='" + rows[i].codigo + "' value='" + rows[i].codigo + "'></td>";
                        newRows += "<td class='cell'><input type='text' class='cell' name='nombre' oldValue='" + rows[i].nombre + "' value='" + rows[i].nombre + "'></td>";
                        newRows += "<td><input type='number' class='cell' name='precio' oldValue='" + rows[i].precio + "' value='" + rows[i].precio + "'></td>";
                        newRows += "<td><input type='number' class='cell' name='existencia' oldValue='" + rows[i].existencia + "' value='" + rows[i].existencia + "'></td>";
                        newRows += '<td id="' + rows[i].id + '"><a class="boton1" href="#">ELIMINAR</a></td></tr>';
                    }
                }
                newRows += "<br>";
                document.getElementById("tbodyList").innerHTML = newRows;
            }           
            var auxiliar = "<br>";
            for (var i = 1; i <= pages; i++) {
                auxiliar += '<a href="#" class="boton">' + i + '</a>';
            }
            document.getElementById("paginador").innerHTML = auxiliar;
        }, 'json');
    };//fin de la funcionn loadDoc
    loadDoc(1);
    
    $("body").on("click", "a.boton", function () {
        loadDoc($(this).html());
    });

    $("body").on("click", ".boton1", function (e) {
        e.preventDefault();
        var paginaActual =$("#oculto").attr("value");
        var id = $(this).parent().attr("id");
        if (confirm("La acción que vas a hacer es permanente. ¿Estás seguro de que quieres eliminar ese dato!")){
            delDoc(id, paginaActual);
        }
        
        //delDoc(id, paginaActual);
    });
    
    var delDoc = function (delRow, currentPage)
    {     
        var idRow = delRow;
        var loadPage = currentPage;
        var url = 'http://localhost/Proyectotienda/es/product/ajaxDelete/';
        $.post(url, {
            id: idRow          
        }, function (data) {
           loadDoc(loadPage);        
        });
    };//fin delDoc 
    
    $("body").on("click", ".boton2", function (e) {
        e.preventDefault();       
        insertProduct();
    });
    
    var insertProduct = function (idRow, lastPage)
    {        
        var page = $("#oculto2").attr("value");
        var codigo = $("#codigo").attr("value");
        var nombre = $("#nombre").attr("value");
        var precio = $("#precio").attr("value");
        var existencia= $("#existencia").attr("value");
        
        var url = 'http://localhost/Proyectotienda/es/product/ajaxInsert/';
        $.post(url, {
            codigo: codigo,
            nombre: nombre,
            precio: precio,
            existencia: existencia
        }, function (data){
            
            loadDoc(page);
           $("#codigo").attr("value", '');
           $("#nombre").attr("value", '');
           $("#precio").attr("value", '');
           $("#existencia").attr("value", '');
           alert("Los datos se han insertado correctamente en la Base de Datos");            
        });
    };

    $("body").on("focusout", "input.cell", function () {
        saveCell(this);
    });

    var saveCell = function (celda)
    {
        var idRow = $(celda).parent().parent().attr("id");
        var column = $(celda).attr("name");
        var value = $(celda).attr("value");
        var oldValue = $(celda).attr("oldValue");
        var url = 'http://localhost/Proyectotienda/es/product/ajaxUpdate/';
        $.post(url, {
            id: idRow,
            campo: column,
            value: value
        }, function () {});
    };//fin saveCell    
    
    $(document).on("click", '.boton4', function () {   
        nuevoPedido(this);
    });
    
    var nuevoPedido = function (botonComprar) {
        var idProd = $(botonComprar).attr("id");
        var idNombre = $(botonComprar).attr("nombre"); 
        var idPrecio = $(botonComprar).attr("precio");             
        $.post('http://localhost/Proyectotienda/es/product/ajaxNuevoPedido',{
            id: idProd, 
            nombre: idNombre, 
            precio: idPrecio}
        , function (data) {
                var confirmado = confirm("En cuanto acepte su producto <<" + $(botonComprar).attr("nombre") + ">> se añadirá al carrito");
                if(confirmado){
                    $("#botonCarrito").removeClass("registroNo");
                    $("#botonCarrito").addClass("registroSi");
                    alert("<<"+$(botonComprar).attr("nombre") + ">> añadido al carrito");
                }
        }, 'json');
        
        $.post('http://localhost/Proyectotienda/es/order/ajaxCantCarrito',""
        , function (data) {
            $("#botonCarrito").html("Carrito: "+data);
          
        });
    };    

    $(document).on("click", '.boton6', function () {        
        $(".listadoPedidos").empty();
        // addPedido();
    });
    
   /* var addPedido = function () {
        $.post('http://localhost/Proyectotienda/es/order/ajaxAddOrder',"", function (data) {
            alert("22");
            if(data){
                $(".listadoPedidos").empty();
            }
        }, 'json');
    };*/
   
    
});
