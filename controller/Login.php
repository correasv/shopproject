<?php

    require_once 'lib/Controller.php';

    class Login extends Controller{      

        function __construct()
        {
            parent::__construct('Login');
        }

        function index(){        
            $this->view->render();        
        }

        function access(){        
            $row = $_POST;  
            $row['password'] = md5($row['password']);        
            $loginRow=$this->model->check($row);   

           if(isset($loginRow[0]['idRole'])){          
                $_SESSION['accessLevel'] = $loginRow[0]['idRole'];
                $_SESSION['name'] = $loginRow[0]['nombre'];
                $_SESSION['idUsuario'] = $loginRow[0]['id'];                
                header('Location: ' . Config::URL . $_SESSION['lang'] . '/' . $_SESSION['currentUrl']);               
           }else{           
            header('Location: ' . Config::URL . $_SESSION['lang'] . '/login');      
           }
        }   

        function unlogin(){           
            session_destroy();
            if($_SESSION['currentUrl'] == 'User'){
                 header('Location: ' . Config::URL . $_SESSION['lang'] .  "/index");  
            }else{                
                header('Location: ' . Config::URL . $_SESSION['lang'] .  "/". $_SESSION['currentUrl']);        
            }
        }
    }
