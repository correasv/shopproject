<?php

require_once 'lib/Controller.php';

class Product extends Controller
{
    function __construct()
    {
        parent::__construct('Product');
    }

    public function index()
    {
        //mostrar lista de todos los registros.
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }

    public function ajaxPageData($pageNumber)
    {       
        $page = $this->model->ajaxPageData($pageNumber);
        echo json_encode($page);
    }

    public function ajaxUpdate()
    {
        $recogidoAjax = $_POST;       
        $this->model->update($recogidoAjax);
    }
    
    public function ajaxDelete()
    {        
        $recogidoAjax = $_POST;        
        $this->model->ajaxDeleteProduct($recogidoAjax);
    }

    public function ajaxInsert()
    {
        $row = $_POST;
        $this->model->insert($row);        
    }
    
    public function ajaxNuevoPedido() {
        $pedido['id'] = $_POST['id'];
        $pedido['nombre'] = $_POST['nombre'];
        $pedido['precio'] = $_POST['precio'];
        $pedido['cantidad']= 1;
        if ($pedido['id'] != "") {
            $_SESSION['listaPedido'][$pedido['id']] = $pedido;            
        }
        echo json_encode($_SESSION['listaPedido']);
    }
    
}
