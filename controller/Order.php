<?php

require_once 'lib/Controller.php';

class Order extends Controller
{
    
    public function __construct() 
    {
        parent::__construct('Order');
    }
    
    public function index()
    {
        $this->view->render();
    }
    
    public function ajaxDeleteOrder($idProduct)
    {            
      unset($_SESSION['listaPedido'][$idProduct]);
      if(count($_SESSION['listaPedido'])==0){
            header('Location: ' . Config::URL . $_SESSION['lang'] . '/product');  
       }else{
           header('Location: ' . Config::URL . $_SESSION['lang'] . '/order');  
       }     
    }
    
    public function ajaxCleanOrder()
    {        
       unset($_SESSION['listaPedido']);
       header('Location: ' . Config::URL . $_SESSION['lang'] . '/product');             
    }
    
    public function ajaxCantCarrito()
    {
        echo count($_SESSION["listaPedido"]);
    }
    
    public function ajaxAddOrder()
    {       
        $option = $this->model->addOrder();       
        $opcion = $this->model->addOrderProducts($option);
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/product'); 
     //   echo json_encode($opcion);        
    }
    
    
}
