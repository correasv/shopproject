<?php
require_once 'lib/Controller.php';
require_once 'model/RoleModel.php';

class User extends Controller
{

    public function __construct()
    {
        parent::__construct('User');
    }   
   
    public function index()
    {
        if(($_SESSION['accessLevel'] != 2)){
           $rows = $this->model->getAll();
           $this->view->render($rows);
        }else{
            
           $this->index2();
        }
    }
    
    public function index2()
    {   
        $id =  $_SESSION['idUsuario'];
        $row = $this->model->get($id);
        $pedidos = ($this->model->pedidos($id));
        $this->view->simpleRender($row, $pedidos);
    }
    
    public function add($repetido='', $error='')
    {
        $roleModel = new RoleModel();
        $roles = $roleModel->getAll(false);        
        $this->view->add($roles, $repetido, $error);
    }
    
    public function insert()
    {
        $row = $_POST;
        
        $comprobacion = $this->model->comprobacion($row['name']);
        
        if($comprobacion['nombre'] == $row['name']){
            $repetido = TRUE;
            $this->add($repetido, $error='');            
        }else{
            $error = $this->_validate($row);
            if (count($error)){
                $this->add($repetido='', $error);
            }
            else{
               $row['password'] = md5($row['password']);        
               $this->model->insert($row);    
               header('Location: ' . Config::URL . $_SESSION['lang'] . '/login');
            }                     
        }       
    }
    
    public function delete($id)
    {
        $this->model->delete($id);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/user/index');
    }
    
    public function edit($id, $error="")
    {
        $roleModel = new RoleModel();
        $roles = $roleModel->getAll(false);        
        $row = $this->model->get($id);        
        $this->view->edit($row, $error, $roles);
    }

    public function update()
    {
        $row = $_POST; 
        $error = $this->_validate($row);
        if (count($error)){
            $this->edit($row['id'], $error);
        }
        else{
            $row['password'] = md5($row['password']);
            $_SESSION['name'] = $row['name'];
            $this->model->update($row);    
            header('Location: ' . Config::URL . $_SESSION['lang'] . '/user/index');
        }
    }
    
    private function _validate($row)
    {
        $error = array();
               
        if (!preg_match("/^.{6,20}$/", $row['password'])){
            $error['password'] = 'error_password';
        }        
        return $error;
    }
    
}
