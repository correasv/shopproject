<?php
require_once 'lib/Controller.php';

class Error extends Controller
{

    public function __construct()
    {
        parent::__construct('Error');
    }

    public function showMessage($message)
    {
        echo $message;
    }
    
    public function index()
    {
        $this->view->render();
    }

}
