<?php /* Smarty version 3.1.27, created on 2016-03-09 00:27:07
         compiled from "template\order.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1652056df5fcb73bee3_31674822%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e4216063671e570233fdbf9bf99bb853a6f02e0b' => 
    array (
      0 => 'template\\order.tpl',
      1 => 1457470020,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1652056df5fcb73bee3_31674822',
  'variables' => 
  array (
    'language' => 0,
    'listaPedido' => 0,
    'row' => 0,
    'totalPrecio' => 0,
    'botonPagar' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56df5fcb7cc789_96886132',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56df5fcb7cc789_96886132')) {
function content_56df5fcb7cc789_96886132 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1652056df5fcb73bee3_31674822';
echo $_smarty_tpl->getSubTemplate ("template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"encabezado"), 0);
?>

<div id="content">
    <br>
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('order_list');?>
</h2>
    <table>
        <tr>            
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('precio');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('existencia');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('total');?>
</th>            
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
        </tr>
        <tbody id="tbody">
        <?php
$_from = $_smarty_tpl->tpl_vars['listaPedido']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
            <tr id="ped<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" class="listadoPedidos">
                <td class="cell" ><?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
</td>
                <td class="cell" ><?php echo $_smarty_tpl->tpl_vars['row']->value['precio'];?>
 €</td>
                <td class="cell" ><input type="number" name="quantity" class="cantidad" precio="<?php echo $_smarty_tpl->tpl_vars['row']->value['precio'];?>
" ide="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" min="1" max="200" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['cantidad'];?>
"></td>
                <td class="cell"  id="total<?php ob_start();
echo $_smarty_tpl->tpl_vars['row']->value['id'];
$_tmp1=ob_get_clean();
echo $_tmp1;?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['cantidad']*$_smarty_tpl->tpl_vars['row']->value['precio'];?>
 €</td>
                <td class="cell" ><a href="http://localhost/proyectotienda/es/order/ajaxDeleteOrder/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" class="boton5">Eliminar</a></td>
            </tr>
             <?php $_smarty_tpl->tpl_vars['totalPrecio'] = new Smarty_Variable($_smarty_tpl->tpl_vars['totalPrecio']->value+($_smarty_tpl->tpl_vars['row']->value['precio']*$_smarty_tpl->tpl_vars['row']->value['cantidad']), null, 0);?>
        <?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
              <tr>
                  <td>Total</td>
                  <td><?php echo $_smarty_tpl->tpl_vars['totalPrecio']->value;?>
 €</td>
                  <td></td>
                  <td></td>
                  <td><?php echo $_smarty_tpl->tpl_vars['botonPagar']->value;?>
</td>                  
              </tr>
              <tr>
                  <td></td>
                  <td></td>
                  <td> </td>
                  <td></td>
                  <td><a href="http://localhost/Proyectotienda/es/order/ajaxCleanOrder" class="boton7">Limpiar carrito</a></td>
              </tr>
        </tbody>
        <tfoot> 
        </tfoot>
    </table> 
</div>
<?php echo $_smarty_tpl->getSubTemplate ("template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0);

}
}
?>