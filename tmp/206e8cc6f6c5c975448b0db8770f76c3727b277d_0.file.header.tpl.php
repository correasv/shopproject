<?php /* Smarty version 3.1.27, created on 2016-03-09 00:22:18
         compiled from "C:\wamp\www\Proyectotienda\template\header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:62256df5eaae0c0d8_43518981%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '206e8cc6f6c5c975448b0db8770f76c3727b277d' => 
    array (
      0 => 'C:\\wamp\\www\\Proyectotienda\\template\\header.tpl',
      1 => 1457467464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '62256df5eaae0c0d8_43518981',
  'variables' => 
  array (
    'language' => 0,
    'url' => 0,
    'js' => 0,
    'file' => 0,
    'lang' => 0,
    'registro1' => 0,
    'user' => 0,
    'registro3' => 0,
    'carrito' => 0,
    'cantidadCarrito' => 0,
    'href' => 0,
    'login' => 0,
    'newUser' => 0,
    'registro2' => 0,
    'mensaje' => 0,
    'name2' => 0,
    'idUsuario' => 0,
    'nombreUsuario' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56df5eaae75873_83621229',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56df5eaae75873_83621229')) {
function content_56df5eaae75873_83621229 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '62256df5eaae0c0d8_43518981';
?>
<!DOCTYPE html>

<html>
    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('online_shop');?>
</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/css/default.css" />
        
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/jquery.js" type="text/javascript"><?php echo '</script'; ?>
>
       <!-- <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/ajaxProducts.js" type="text/javascript"><?php echo '</script'; ?>
> -->
        <!-- SI EXISTEN MAS FICHEROS ASIGNADOS A LA VISTA, LOS CARGARA AUTOMATICAMENTE -->
        <?php if (isset($_smarty_tpl->tpl_vars['js']->value) && count($_smarty_tpl->tpl_vars['js']->value)) {?>            
            <?php
$_from = $_smarty_tpl->tpl_vars['js']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['file']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->_loop = true;
$foreach_file_Sav = $_smarty_tpl->tpl_vars['file'];
?>                
               <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/<?php echo $_smarty_tpl->tpl_vars['file']->value;?>
" type="text/javascript"><?php echo '</script'; ?>
>                 
            <?php
$_smarty_tpl->tpl_vars['file'] = $foreach_file_Sav;
}
?>            
        <?php }?>
            
    </head>
    <body>
        <div id="header">
            <div id="title">
                <?php echo $_smarty_tpl->tpl_vars['language']->value->translate('online_shop');?>
 - <?php echo $_smarty_tpl->tpl_vars['language']->value->translate('language');?>

                <br>   
            </div>
            <div  style="float: left">
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/index" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('index');?>
</a> 
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/help" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('help');?>
</a>
                <a class="<?php echo $_smarty_tpl->tpl_vars['registro1']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user" ><?php echo $_smarty_tpl->tpl_vars['user']->value;?>
</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/product" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('product');?>
</a>
                <a id="botonCarrito" class="<?php echo $_smarty_tpl->tpl_vars['registro1']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['registro3']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/order" ><?php echo $_smarty_tpl->tpl_vars['carrito']->value;?>
 : <?php echo $_smarty_tpl->tpl_vars['cantidadCarrito']->value;?>
</a>               
            </div>
            <div  style="float: left ; padding-left:4em">
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/es/index">ES</a> 
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/en/index" >EN</a>
              <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;
echo $_smarty_tpl->tpl_vars['href']->value;?>
" ><?php ob_start();
echo $_smarty_tpl->tpl_vars['login']->value;
$_tmp1=ob_get_clean();
echo $_smarty_tpl->tpl_vars['language']->value->translate($_tmp1);?>
</a>
              <?php echo $_smarty_tpl->tpl_vars['newUser']->value;?>

              <a class="<?php echo $_smarty_tpl->tpl_vars['registro2']->value;?>
" href="#"><?php echo $_smarty_tpl->tpl_vars['mensaje']->value;
echo $_smarty_tpl->tpl_vars['name2']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['idUsuario']->value;?>
</a>
              <?php echo $_smarty_tpl->tpl_vars['nombreUsuario']->value;?>

            </div>
        </div>

            
<?php }
}
?>