<?php /* Smarty version 3.1.27, created on 2016-03-09 00:36:19
         compiled from "template\product.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2603556df61f3590748_88876960%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'edafb031939d0136ab5fa9937f06011955ee7911' => 
    array (
      0 => 'template\\product.tpl',
      1 => 1457479822,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2603556df61f3590748_88876960',
  'variables' => 
  array (
    'language' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56df61f35daad2_15562620',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56df61f35daad2_15562620')) {
function content_56df61f35daad2_15562620 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2603556df61f3590748_88876960';
echo $_smarty_tpl->getSubTemplate ("template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"encabezado"), 0);
?>

<div id="content">
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('product_list');?>
</h2>
    
    
    <table><thead id="theadList">
        <tr>   
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('codigo');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('nombre');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('precio');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('existencia');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operaciones');?>
</th>           
        </tr>
        </thead>
        <tfoot id="tfootList">
            <tr class="cell2">
                <td class="cell"><input id="codigo" placeholder="" class="cell" type="text"></td>
                <td class="cell"><input id="nombre" placeholder="" class="cell" type="text"></td>
                <td class="cell"><input id="precio" placeholder="" class="cell" type="text"></td>
                <td class="cell"><input id="existencia" placeholder="" class="cell" type="text"></td>
                <td class="new"><a class="boton2" href="#">NUEVO</a></td>
            </tr>  
        </tfoot>        
        <tbody  id="tbodyList">      
        </tbody>
    </table>
    <div id="paginador"></div>
    
   

 </div>
<?php echo $_smarty_tpl->getSubTemplate ("template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0);

}
}
?>