<?php /* Smarty version 3.1.27, created on 2016-03-09 00:31:29
         compiled from "template\user.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3037556df60d1f17b50_15259824%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d0ece3b320736edd9046851475f946797b88337' => 
    array (
      0 => 'template\\user.tpl',
      1 => 1457479822,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3037556df60d1f17b50_15259824',
  'variables' => 
  array (
    'language' => 0,
    'rows' => 0,
    'row' => 0,
    'url' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56df60d204e8f1_23412058',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56df60d204e8f1_23412058')) {
function content_56df60d204e8f1_23412058 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3037556df60d1f17b50_15259824';
echo $_smarty_tpl->getSubTemplate ("template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"encabezado"), 0);
?>

<div id="content">
    <br>
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('user_list');?>
</h2>    
    <table>
        <tr>
            <th>Id</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('password');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('role');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
        </tr>
        
        <?php
$_from = $_smarty_tpl->tpl_vars['rows']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
            <tr>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['password'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['role'];?>
</td>
                <td>
                    <a class="botonOperacion" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('edit');?>
</a>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['id'] != 1) {?>
                    <a class="botonOperacion" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/delete/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('delete');?>
</a>                    
                    <?php }?>
                </td>
            </tr>
        <?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
    </table>    
 </div>
<?php echo $_smarty_tpl->getSubTemplate ("template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0);

}
}
?>