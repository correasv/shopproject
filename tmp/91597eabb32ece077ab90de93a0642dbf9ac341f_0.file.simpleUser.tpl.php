<?php /* Smarty version 3.1.27, created on 2016-03-09 00:36:32
         compiled from "template\simpleUser.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3146556df62006d3b02_89998298%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '91597eabb32ece077ab90de93a0642dbf9ac341f' => 
    array (
      0 => 'template\\simpleUser.tpl',
      1 => 1457479822,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3146556df62006d3b02_89998298',
  'variables' => 
  array (
    'language' => 0,
    'row' => 0,
    'pedido' => 0,
    'url' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56df62007355a6_72311192',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56df62007355a6_72311192')) {
function content_56df62007355a6_72311192 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3146556df62006d3b02_89998298';
echo $_smarty_tpl->getSubTemplate ("template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"encabezado"), 0);
?>

<div id="content">
    <br>
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('my_profile');?>
</h2>    
    <table>
        <tr>            
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('password');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('role');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('orders');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
        </tr>
        
        <tr>            
            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['password'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['role'];?>
</td>
            <td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['pedido']->value;?>
</td>
            <td>
                <a class="botonOperacion" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('edit');?>
</a>
                <a class="botonOperacion" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/delete/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('delete');?>
</a>                    
            </td>
        </tr>      
    </table>  
         
 </div>
<?php echo $_smarty_tpl->getSubTemplate ("template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0);

}
}
?>