{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('user_list')}</h2>    
    <table>
        <tr>
            <th>Id</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('password')}</th>
            <th>{$language->translate('role')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
        
        {foreach $rows as $row}
            <tr>
                <td>{$row.id}</td>
                <td>{$row.nombre}</td>
                <td>{$row.password}</td>
                <td>{$row.role}</td>
                <td>
                    <a class="botonOperacion" href="{$url}{$lang}/user/edit/{$row.id}" >{$language->translate('edit')}</a>
                    {if $row.id != 1}
                    <a class="botonOperacion" href="{$url}{$lang}/user/delete/{$row.id}" >{$language->translate('delete')}</a>                    
                    {/if}
                </td>
            </tr>
        {/foreach}
    </table>    
 </div>
{include file="template/footer.tpl" title="footer"}