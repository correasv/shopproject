<!DOCTYPE html>

<html>
    <head>
        <title>{$language->translate('online_shop')}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{$url}public/css/default.css" />
        
        <script src="{$url}public/js/jquery.js" type="text/javascript"></script>
       <!-- <script src="{$url}public/js/ajaxProducts.js" type="text/javascript"></script> -->
        <!-- SI EXISTEN MAS FICHEROS ASIGNADOS A LA VISTA, LOS CARGARA AUTOMATICAMENTE -->
        {if isset($js) && count($js)}            
            {foreach item=file from = $js}                
               <script src="{$url}public/js/{$file}" type="text/javascript"></script>                 
            {/foreach}            
        {/if}
            
    </head>
    <body>
        <div id="header">
            <div id="title">
                {$language->translate('online_shop')} - {$language->translate('language')}
                <br>   
            </div>
            <div  style="float: left">
                <a href="{$url}{$lang}/index" >{$language->translate('index')}</a> 
                <a href="{$url}{$lang}/help" >{$language->translate('help')}</a>
                <a class="{$registro1}" href="{$url}{$lang}/user" >{$user}</a>
                <a href="{$url}{$lang}/product" >{$language->translate('product')}</a>
                <a id="botonCarrito" class="{$registro1} {$registro3}" href="{$url}{$lang}/order" >{$carrito} : {$cantidadCarrito}</a>               
            </div>
            <div  style="float: left ; padding-left:4em">
                <a href="{$url}/es/index">ES</a> 
                <a href="{$url}/en/index" >EN</a>
              <a href="{$url}{$lang}{$href}" >{$language->translate({$login})}</a>
              {$newUser}
              <a class="{$registro2}" href="#">{$mensaje}{$name2} {$idUsuario}</a>
              {$nombreUsuario}
            </div>
        </div>

            
