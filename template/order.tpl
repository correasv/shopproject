{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('order_list')}</h2>
    <table>
        <tr>            
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('precio')}</th>
            <th>{$language->translate('existencia')}</th>
            <th>{$language->translate('total')}</th>            
            <th>{$language->translate('operations')}</th>
        </tr>
        <tbody id="tbody">
        {foreach $listaPedido as $row}
            <tr id="ped{$row['id']}" class="listadoPedidos">
                <td class="cell" >{$row['nombre']}</td>
                <td class="cell" >{$row['precio']} €</td>
                <td class="cell" ><input type="number" name="quantity" class="cantidad" precio="{$row['precio']}" ide="{$row['id']}" min="1" max="200" value="{$row['cantidad']}"></td>
                <td class="cell"  id="total{{$row['id']}}">{$row['cantidad']*$row['precio']} €</td>
                <td class="cell" ><a href="http://localhost/proyectotienda/es/order/ajaxDeleteOrder/{$row['id']}" class="boton5">Eliminar</a></td>
            </tr>
             {$totalPrecio = $totalPrecio + ($row['precio']*$row['cantidad'])}
        {/foreach}
              <tr>
                  <td>Total</td>
                  <td>{$totalPrecio} €</td>
                  <td></td>
                  <td></td>
                  <td>{$botonPagar}</td>                  
              </tr>
              <tr>
                  <td></td>
                  <td></td>
                  <td> </td>
                  <td></td>
                  <td><a href="http://localhost/Proyectotienda/es/order/ajaxCleanOrder" class="boton7">Limpiar carrito</a></td>
              </tr>
        </tbody>
        <tfoot> 
        </tfoot>
    </table> 
</div>
{include file="template/footer.tpl" title="footer"}