{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('product_list')}</h2>
    
    
    <table><thead id="theadList">
        <tr>   
            <th>{$language->translate('codigo')}</th>
            <th>{$language->translate('nombre')}</th>
            <th>{$language->translate('precio')}</th>
            <th>{$language->translate('existencia')}</th>
            <th>{$language->translate('operaciones')}</th>           
        </tr>
        </thead>
        <tfoot id="tfootList">
            <tr class="cell2">
                <td class="cell"><input id="codigo" placeholder="" class="cell" type="text"></td>
                <td class="cell"><input id="nombre" placeholder="" class="cell" type="text"></td>
                <td class="cell"><input id="precio" placeholder="" class="cell" type="text"></td>
                <td class="cell"><input id="existencia" placeholder="" class="cell" type="text"></td>
                <td class="new"><a class="boton2" href="#">NUEVO</a></td>
            </tr>  
        </tfoot>        
        <tbody  id="tbodyList">      
        </tbody>
    </table>
    <div id="paginador"></div>
    
   

 </div>
{include file="template/footer.tpl" title="footer"}