{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('my_profile')}</h2>    
    <table>
        <tr>            
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('password')}</th>
            <th>{$language->translate('role')}</th>
            <th>{$language->translate('orders')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
        
        <tr>            
            <td>{$row.nombre}</td>
            <td>{$row.password}</td>
            <td>{$row.role}</td>
            <td style="text-align: center;">{$pedido}</td>
            <td>
                <a class="botonOperacion" href="{$url}{$lang}/user/edit/{$row.id}" >{$language->translate('edit')}</a>
                <a class="botonOperacion" href="{$url}{$lang}/user/delete/{$row.id}" >{$language->translate('delete')}</a>                    
            </td>
        </tr>      
    </table>  
         
 </div>
{include file="template/footer.tpl" title="footer"}