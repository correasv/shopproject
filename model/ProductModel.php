<?php
require_once 'lib/Model.php';

class ProductModel extends Model{
    
    const PAGE_SIZE = 3; 
    
    function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->_sql = "DELETE FROM producto WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id)
    {      
    }

    public function getAll()
    {
        $this->_sql = "SELECT * FROM producto ORDER BY id";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila)
    {
        $this->_sql = "INSERT INTO producto (codigo, nombre, precio, existencia)"
                . " VALUES ('$fila[codigo]', '$fila[nombre]', '$fila[precio]', '$fila[existencia]')";

        $this->executeQuery();
    }

    public function update($row)
    {       
        $this->_sql = "UPDATE producto SET "
                . " $row[campo]='$row[value]' "               
                . " WHERE id = $row[id]";
        
        $this->executeQuery();
    }
    
    public function ajaxPageData($pageNumber)
    {
        $this->_sql = "SELECT CEILING (COUNT(id)/" . $this::PAGE_SIZE . ") AS pages  FROM producto" ;
        $this->executeSelect();
        $page[] = $pageNumber;
        $page[] = $this->_rows[0]['pages'];
        $this->_sql ="SELECT producto.*  FROM producto ORDER BY id LIMIT " . $this::PAGE_SIZE * ($pageNumber-1) . ','. $this::PAGE_SIZE;
        $this->executeSelect();
        $page[] = $this->_rows;
        $page[] = $_SESSION['accessLevel'];
        return $page;
    }
    
    public function ajaxDeleteProduct($id)
    {
        var_dump($id);
        $this->_sql = "DELETE FROM producto where id = $id[id]";
        $this->executeQuery();
    }
    
   public function ajaxAddProduct($id)
    {
        $row['id'] = $id;
        $row['count'] =  1;
        $row['prize'] = function_get_price;        
        $SESSION['basket'][$id] = $linea;
    }
    
}