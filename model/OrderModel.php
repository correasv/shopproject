<?php

require_once 'lib/Model.php';

class OrderModel extends Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function addOrder() {
        $idUser = $_SESSION['idUsuario'];
        $this->_sql = "INSERT INTO pedido(fechaServido, estado, idUsuario) "
                . "VALUES (0, 0,'" . $idUser . "')";
        if ($this->executeQuery()) {
             $this->_sql = "SELECT id FROM pedido ORDER BY id DESC LIMIT 1" ;
             $this->executeSelect();
             return $this->_rows[0][id];
        } else {
            return false;
        }
    }
    
    public function addOrderProducts($idPedido) {

        $arrayProd = $_SESSION['listaPedido'];
        $idNueva = 0;
        $contador = 0;
        foreach ($arrayProd as $producto) {
            $contador++;       
            $idP = $producto['id'];
            $precioP = $producto['precio'];
            $cantidadP = $producto['cantidad'];            
            $this->_sql = "INSERT INTO detallepedido (idPedido, linea, idProducto, cantidad, precio)"
                   . " VALUES ('" . $idPedido."','". $contador ."', '".$idP."', '".$cantidadP."', '".$precioP."')";

           $this->executeQuery();
        }
        unset($_SESSION['listaPedido']);
        return true;
    }   
    
    protected function delete($numero) {}

    protected function get($numero) {}

    protected function getAll() {}

    protected function insert($fila) {}

    protected function update($fila) {}

}
