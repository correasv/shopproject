<?php
require_once 'lib/View.php';

class ErrorView extends View
{

    function __construct()
    {
        parent::__construct();
    }

    public function render(Exception $ex)
    {
        $plantilla='error.tpl';
         $this->smarty->assign('img', '<img src="http://localhost/Proyectotienda/img/error.png">'); 
        $this->smarty->assign('ex', $ex);        
        $this->smarty->display($plantilla);        
    }
}