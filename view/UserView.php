<?php

require_once 'lib/View.php';

class UserView extends View
{
    function __construct()
    {
        parent::__construct();
    }

    public function render($rows)
    {       
        $template='user.tpl';
        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
       
    }
    
    public function simpleRender($row, $pedidos=0)
    {
        $template='simpleUser.tpl';
         $this->smarty->assign('pedido', $pedidos);
        $this->smarty->assign('row', $row);
        $this->smarty->display($template);
    }
    
    public function add($roles, $repetido, $error='')
    {

        $template='userFormAdd.tpl';
        $this->smarty->assign('roles', $roles);
        $this->smarty->assign('alta', $this->lang->translate('alta'));
        $this->smarty->assign('name', $this->lang->translate('name'));
        $this->smarty->assign('password', $this->lang->translate('password'));
        $this->smarty->assign('error', $error);
        if($repetido == TRUE){            
            $this->smarty->assign('repetido', $this->lang->translate('repetido'));
        }
        $this->smarty->display($template);
    }
    
    public function edit($row, $error="", $roles)
    {   
        if($_SESSION['accessLevel'] == 2){
            $template='userFormEditUser.tpl';
            $this->smarty->assign('row', $row);
            $this->smarty->assign('edicion', $this->lang->translate('edicion'));
            $this->smarty->assign('nuevoPass', $this->lang->translate('nuevoPass'));
            $this->smarty->display($template);
        }
        else{
            $template='userFormEdit.tpl';
             $this->smarty->assign('edicionUsuario',$this->lang->translate('edicionUsuario'));
            $this->smarty->assign('error', $error);
            $this->smarty->assign('row', $row);
            $this->smarty->assign('roles', $roles);
            $this->smarty->display($template);
        }        
    }
    
    
}
