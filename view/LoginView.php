<?php

require_once 'lib/View.php';

class LoginView extends View
{   
    function __construct()
    {
        parent::__construct();
    }
    
    public function render()
    {       
        $template='loginForm.tpl';   
        $this->smarty->display($template);
    }
}
