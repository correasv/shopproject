<?php

require_once 'lib/View.php';

class OrderView extends View
{
    public function __construct() {
        parent::__construct();
    }
    
    public function render()
    {      
        if(count($_SESSION['listaPedido'])!=0){
            $plantilla ='order.tpl';
            $js[] = 'ajaxProducts.js';
            $this->smarty->assign('js', $js);
            $this->smarty->assign('listaPedido', $_SESSION['listaPedido'] );
            $this->smarty->display($plantilla);
         
        }else{
             $this->smarty->display('noPedido.tpl');       
        }                
    }
}
