<?php

require_once 'lib/View.php';

class ProductView extends View
{
    function __construct()
    {
        parent::__construct();
    }

    public function render($rows, $template='product.tpl')
    {
        $js[] = 'ajaxProducts.js';
        $this->smarty->assign('js', $js);
        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }
}
