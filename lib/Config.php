<?php
abstract class Config{
    const URL = 'http://localhost/shopProject/';   
    const DBHOST = 'localhost';
    const DBUSER = 'root';
    const DBPASSWORD = '';
    const DBNAME = 'shop';
    
    const DEFAULT_LANG = 'en';
}