<?php
require_once 'smarty/libs/Smarty.class.php';
require_once 'lib/Lang.php';

class View{
    public $smarty;
    private $_method;
    public $lang;    
   
    function __construct()
    {
        $this->lang = new Lang();        
        //preparar smarty
        $this->smarty = new Smarty();
        $this->smarty->template_dir = 'template';
        $this->smarty->compile_dir = 'tmp';
       
       if($_SESSION['accessLevel'] >= 2){
        $this->smarty->assign("operaciones", "<th>" . $this->lang->translate('operations'). "</th>");
       }else{
            $this->smarty->assign("operaciones", '');
       }
        //asignar variables para las cabeceras/pies
        $this->smarty->assign('lang', $_SESSION['lang']);       
     //   $this->smarty->assign('current', $_SESSION['currentUrl']);
       if($_SESSION['accessLevel'] == 1){
              $this->smarty->assign('href', '/login');   
               $this->smarty->assign('login', 'login');   
        }else{
              $this->smarty->assign('href', '/login/unlogin');   
              $this->smarty->assign('login', 'unlogin');                          
        }
        
        
        if($_SESSION['accessLevel'] == 1){             
               $this->smarty->assign('newUser', '<a href="'.Config::URL.$_SESSION["lang"].'/user/add">'.
                                      $this->lang->translate('new_user').'</a>');   
              $this->smarty->assign('registro1', 'registroNo'); 
              $this->smarty->assign('registro2', 'registroNo');
        }else{
              $this->smarty->assign('newUser', '');           
              $this->smarty->assign('registro1', 'registroSi');
              $this->smarty->assign('mensaje', $this->lang->translate('Bienvenido:'));              
              $this->smarty->assign('name2', $_SESSION['name']);
              $this->smarty->assign('user', $this->lang->translate('user'));
              $this->smarty->assign('carrito', $this->lang->translate('carrito'));              
             
              if(count($_SESSION["listaPedido"])==0){     
                    $this->smarty->assign('botonPagar','');
                    $this->smarty->assign('registro3', 'registroNo'); 
                }else{
                    $this->smarty->assign('cantidadCarrito', count($_SESSION["listaPedido"])); 
                    $this->smarty->assign('botonPagar','<a href="http://localhost/proyectotienda/es/order/ajaxAddOrder" class="boton6">REALIZAR PEDIDO</a>');
                    $this->smarty->assign('registro3', 'registroSi');              
                }
        }        
        
        $this->smarty->assign('language', $this->lang);
        $this->smarty->assign('url', Config::URL);
        
    }    
    function getMethod()
    {
        return $this->_method;
    }

    function setMethod($method)
    {
        $this->_method = $method;
    }
}