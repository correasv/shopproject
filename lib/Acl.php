<?php

/* Clase con los permisos asiginados a un role:
 * Relación ROLE-RESOURCE */
//1 ->invitado, 2 ->usuario y 3 ->administrador

class Acl{
    private $_acl = array(
        'index' => array(
            'index' => 1
        ),        
        'user' => array(
            'index' => 2,
            'add' => 1,
            'edit' => 3,
            'delete' => 3,
            'update' => 3,
            'insert' => 1
        ),        
        'product' => array(
            'index' => 1,
            'add' => 3,
            'edit' => 3,
            'delete' => 3,
            'update' => 3,
            'insert' => 3
        )
    );
    
    public function __construct()    {
        
    }
    
    public function isAllowed($className, $method, $accessLevel)
    {
        $className = strtolower($className);
        if (isset($this->_acl[$className][$method])){
            return  $accessLevel >= $this->_acl[$className][$method] ; 
        }
        else {return true;}
    }
}
